/*
  Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
*/

/// @author Jackson Burzynski (jackson.carl.burzynski@cern.ch)

#ifndef GNN_DECORATION_ALG_H
#define GNN_DECORATION_ALG_H

#include <AnaAlgorithm/AnaAlgorithm.h>

#include <xAODEventInfo/EventInfo.h>
#include <AsgTools/CurrentContext.h>

#include <xAODJet/JetContainer.h>

#include <AsgTools/PropertyWrapper.h>
#include <AsgDataHandles/WriteHandleKey.h>
#include <AsgDataHandles/WriteDecorHandleKey.h>
#include <AsgDataHandles/ReadHandleKey.h>
#include <AsgDataHandles/ReadDecorHandleKey.h>
#include <AsgDataHandles/WriteHandle.h>
#include <AsgDataHandles/ReadHandle.h>

#include <FlavorTagDiscriminants/GNNTool.h>

#include <string>

namespace EJs
{
  class GNNDecoratorAlg final : public EL::AnaAlgorithm
  {
    /// \brief the standard constructor
  public:
    GNNDecoratorAlg (const std::string& name, 
                     ISvcLocator* pSvcLocator);
    StatusCode initialize () override;
    StatusCode execute () override;
    
  private:
    /// \brief the GNN tool
    ToolHandle<FlavorTagDiscriminants::GNNTool> m_GNNTool{this, "GNNTool", "", "GNN Decorator tool"};

    // Input Containers
    SG::ReadHandleKey< xAOD::JetContainer > m_JetContainerKey {
      this,"jetContainer","AntiKt10EMTopoRCJets",
        "Key for the input jet collection"};
  };
}

#endif // GNN_DECORATION_ALG_H

