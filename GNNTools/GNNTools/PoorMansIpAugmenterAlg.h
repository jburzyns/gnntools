/*
  Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
*/

#ifndef POOR_MANS_TRACK_AUGMENTER_ALG_HH
#define POOR_MANS_TRACK_AUGMENTER_ALG_HH

#include <xAODTracking/TrackParticleContainer.h>
#include <xAODEventInfo/EventInfo.h>
#include <AsgTools/CurrentContext.h>

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <xAODTracking/VertexContainer.h>
#include <xAODTracking/VertexAuxContainer.h>

#include <AsgTools/PropertyWrapper.h>
#include <AsgDataHandles/WriteHandleKey.h>
#include <AsgDataHandles/WriteDecorHandleKey.h>
#include <AsgDataHandles/ReadHandleKey.h>
#include <AsgDataHandles/ReadDecorHandleKey.h>
#include <AsgDataHandles/WriteHandle.h>
#include <AsgDataHandles/ReadHandle.h>

#include <string>

namespace EJs {

  class PoorMansIpAugmenterAlg: public EL::AnaAlgorithm {
  public:
    PoorMansIpAugmenterAlg(const std::string& name,
                          ISvcLocator* pSvcLocator );

    StatusCode initialize() override;
    StatusCode execute() override;
    StatusCode finalize() override;


  private:
    const xAOD::Vertex* getPrimaryVertex( const xAOD::VertexContainer& ) const;

  private:
    // Input Containers
    SG::ReadHandleKey< xAOD::TrackParticleContainer > m_TrackContainerKey {
      this,"trackContainer","InDetTrackParticles",
        "Key for the input track collection"};
    SG::ReadHandleKey< xAOD::VertexContainer > m_VertexContainerKey {
      this,"primaryVertexContainer","",
      "Key for the input vertex collection, (empty to use beamspot)"};

    SG::ReadHandleKey< xAOD::EventInfo > m_eventInfoKey {
      this, "eventInfo", "EventInfo", "Key for EventInfo"};

    // Decorators for tracks
    Gaudi::Property< std::string > m_prefix{this,"prefix","btagIp_",""};

    SG::WriteDecorHandleKey< xAOD::TrackParticleContainer > m_dec_d0_sigma {
      this, "d0Uncertainty", "d0Uncertainty", "d0Uncertainty of tracks"};
    SG::WriteDecorHandleKey< xAOD::TrackParticleContainer > m_dec_z0_sigma {
      this, "z0SinThetaUncertainty", "z0SinThetaUncertainty",
      "z0SinThetaUncertainty of tracks"};

    SG::WriteDecorHandleKey< xAOD::TrackParticleContainer > m_dec_track_pos {
      this, "trackDisplacement","trackDisplacement",
      "trackDisplacement of tracks" };
    SG::WriteDecorHandleKey< xAOD::TrackParticleContainer > m_dec_track_mom {
      this, "trackMomentum","trackMomentum","trackMomentum of tracks" };

    SG::WriteDecorHandleKey< xAOD::TrackParticleContainer > m_dec_invalid {
      this, "invalidIp", "invalidIp", "flag for invalid impact parameter"
    };

    // accessors for beam spot uncertainty
    SG::ReadDecorHandleKey<xAOD::EventInfo> m_beam_sigma_x {
      this, "beamspotSigmaX", "EventInfo.beamPosSigmaX",
      "Beam spot position sigma in X"
    };
    SG::ReadDecorHandleKey<xAOD::EventInfo> m_beam_sigma_y {
      this, "beamspotSigmaY", "EventInfo.beamPosSigmaY",
      "Beam spot position sigma in Y"
    };
    SG::ReadDecorHandleKey<xAOD::EventInfo> m_beam_sigma_z {
      this, "beamspotSigmaZ", "EventInfo.beamPosSigmaZ",
      "Beam spot position sigma in Z"
    };
    // note that this last entry is a covariance: the units are mm^2,
    // whereas the above have units of mm
    SG::ReadDecorHandleKey<xAOD::EventInfo> m_beam_cov_xy {
      this, "beamspotCovarianceXY", "EventInfo.beamPosSigmaXY",
      "Beam spot covariance in XY"
    };


  };

}

#endif

