/*
  Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
*/

/// @author Jackson Burzynski (jackson.carl.burzynski@cern.ch)

#ifndef GNN_TOOLS_DICT_H
#define GNN_TOOLS_DICT_H

#include <GNNTools/GNNDecoratorAlg.h>
#include <GNNTools/PoorMansIpAugmenterAlg.h>
#include <GNNTools/JetGhostMergingAlg.h>

#endif // GNN_TOOLS_DICT_H

