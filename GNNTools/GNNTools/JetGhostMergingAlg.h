/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/

/// @author Jackson Burzynski

#ifndef JET_ANALYSIS_ALGORITHMS__JET_GHOST_MERGING_ALG_H
#define JET_ANALYSIS_ALGORITHMS__JET_GHOST_MERGING_ALG_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <xAODJet/JetContainer.h>
#include <AsgTools/CurrentContext.h>

#include <AsgTools/PropertyWrapper.h>
#include <AsgDataHandles/WriteHandleKey.h>
#include <AsgDataHandles/ReadHandleKey.h>
#include <AsgDataHandles/WriteHandle.h>
#include <AsgDataHandles/ReadHandle.h>

#include <string>
#include <vector>

namespace EJs
{
  /// \brief an algorithm for combining multiple ghost collections into one
  class JetGhostMergingAlg final : public EL::AnaAlgorithm
  {
    /// \brief the standard constructor
  public:
    JetGhostMergingAlg (const std::string& name, 
                        ISvcLocator* pSvcLocator);

  public:
    StatusCode initialize () override;

  public:
    StatusCode execute () override;

    /// \brief the jet collection we run on
  private:
    SG::ReadHandleKey<xAOD::JetContainer> m_JetContainerKey{this,"jetContainer","AntiKt4EMTopoJets"};

    /// \brief the names of the input and output ghost collections
  private:
    Gaudi::Property<std::vector<std::string>> m_inputGhostTrackNames{this, "InputGhostTrackNames", {"GhostTrack","GhostTrackLRT"}};
    Gaudi::Property<std::string> m_mergedGhostTrackName{this, "MergedGhostName", "GhostTrackLRTMerged"};
  };
}

#endif // JET_ANALYSIS_ALGORITHMS__JET_GHOST_MERGING_ALG_H
