#!/usr/bin/env python

# Read the submission directory as a command line argument. You can
# extend the list of arguments with your private ones later on.
import optparse
parser = optparse.OptionParser()
parser.add_option( '-s', '--submission-dir', dest = 'submission_dir',
                   action = 'store', type = 'string', default = 'submitDir',
                   help = 'Submission directory for EventLoop' )
( options, args ) = parser.parse_args()

# Set up (Py)ROOT.
import ROOT
ROOT.xAOD.Init().ignore()

# Set up the sample handler object. See comments from the C++ macro
# for the details about these lines.
import os
sh = ROOT.SH.SampleHandler()
sh.setMetaString( 'nc_tree', 'CollectionTree' )
#inputFilePath = '/project/ctb-stelzer/jburzyns/EmergingJets/samples/DAOD_FTAG1/user.jburzyns.801964.Py8EG_Zprime2EJs_Ld20_rho40_pi10_Zp600_l50.deriv.DAOD_FTAG1.e8453_e8455_s3873_s3874_r13829_p3257_EXT0'
#ROOT.SH.ScanDir().filePattern( 'user.jburzyns.32829613.EXT0._000001.DAOD_FTAG1.test.pool.root' ).scan( sh, inputFilePath )
inputFilePath = '/project/ctb-stelzer/jburzyns/EmergingJets/LLP1/run/'
ROOT.SH.ScanDir().filePattern( 'DAOD_LLP1.test.pool.root' ).scan( sh, inputFilePath )
sh.printContent()

# Create an EventLoop job.
job = ROOT.EL.Job()
job.sampleHandler( sh )
job.options().setDouble( ROOT.EL.Job.optMaxEvents, 500 )
job.options().setString( ROOT.EL.Job.optSubmitDirMode, 'unique-link')

# Create the algorithm's configuration.
from AnaAlgorithm.DualUseConfig import createAlgorithm, createReentrantAlgorithm, addPrivateTool

ipAugSTD = createAlgorithm( 'EJs::PoorMansIpAugmenterAlg', 'PoBoy' )
ipAugSTD.trackContainer = "InDetTrackParticles"
job.algsAdd( ipAugSTD )
ipAugLRT = createAlgorithm( 'EJs::PoorMansIpAugmenterAlg', 'PoBoyLRT' )
ipAugLRT.trackContainer = "InDetLargeD0TrackParticles"
job.algsAdd( ipAugLRT )

merger = createAlgorithm( 'EJs::JetGhostMergingAlg', 'JetGhostMerger' )
merger.jetContainer = "AntiKt10EMTopoRCJets"
job.algsAdd( merger )

alg = createAlgorithm( 'EJs::GNNDecoratorAlg', 'GNNDecorator' )
addPrivateTool( alg, 'GNNTool', 'FlavorTagDiscriminants::GNNTool' )
alg.jetContainer = "AntiKt10EMTopoRCJets"
alg.GNNTool.nnFile = "/scratch/ppokhare/network.onnx"
alg.GNNTool.trackLinkType = "IPARTICLE"
alg.GNNTool.variableRemapping = {"BTagTrackToJetAssociator":"GhostTrackLRTMerged",
                                   "numberOfNextToInnermostPixelLayerHits": "numberOfInnermostPixelLayerHits"
                                  }

# later on we'll add some configuration options for our algorithm that go here

# Add our algorithm to the job
job.algsAdd( alg )

# Run the job using the direct driver.
driver = ROOT.EL.DirectDriver()
driver.submit( job, options.submission_dir )
