/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/

/// @author Jackson Burzynski

///////////////////////////////////////////////////////////////////
//   JetGhostMergingAlg
//
//   Ghost merger algorithm merges the a collection of ghost 
//   containers into one. This is useful for combining standard and
//   LRT ghost tracks into a single collection to pass to 
//   downstream taggers.
///////////////////////////////////////////////////////////////////

//
// includes
//
#include <GNNTools/JetGhostMergingAlg.h>

namespace EJs
{
  JetGhostMergingAlg ::
  JetGhostMergingAlg (const std::string& name, 
                      ISvcLocator* pSvcLocator)
    : AnaAlgorithm (name, pSvcLocator)
  {
  }

  StatusCode JetGhostMergingAlg :: 
  initialize ()
  {
    ANA_CHECK (m_JetContainerKey.initialize ());
    return StatusCode::SUCCESS;
  }

  StatusCode JetGhostMergingAlg :: 
  execute ()
  {
    const EventContext &ctx = Gaudi::Hive::currentContext();

    SG::ReadHandle<xAOD::JetContainer> inputJets(m_JetContainerKey,ctx);
    if (!inputJets.isValid()) {
        ATH_MSG_FATAL("No jet collection with name " << m_JetContainerKey.key() << " found in StoreGate!");
        return StatusCode::FAILURE;
    }

    static const SG::AuxElement::Decorator<std::vector<ElementLink<DataVector<xAOD::IParticle> > > > mergedGhostDecor(m_mergedGhostTrackName);

    for(const xAOD::Jet *jet: *inputJets) {
        std::vector<ElementLink<xAOD::IParticleContainer>> mergedGhosts;
        for (const auto& ghostName: m_inputGhostTrackNames) {
            const SG::AuxElement::ConstAccessor<std::vector<ElementLink<DataVector<xAOD::IParticle> > > > ghostTrackAcc(ghostName);
            const std::vector< ElementLink<DataVector<xAOD::IParticle> > > &ghosts = ghostTrackAcc( *jet );
            mergedGhosts.insert(mergedGhosts.end(), ghosts.begin(), ghosts.end());
        }
        mergedGhostDecor(*jet) = mergedGhosts;
    }
    return StatusCode::SUCCESS;
  }

}
