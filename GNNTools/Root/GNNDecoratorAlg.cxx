/*
  Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
*/

/// @author Jackson Burzynski (jackson.carl.burzynski@cern.ch)

#include "GNNTools/GNNDecoratorAlg.h"
#include <TH1.h>

namespace EJs
{
  GNNDecoratorAlg :: GNNDecoratorAlg (const std::string& name, 
                                      ISvcLocator* pSvcLocator)
    : AnaAlgorithm (name, pSvcLocator) {}

  StatusCode GNNDecoratorAlg :: initialize ()
  {

    ATH_MSG_INFO( "Inizializing " << name() << "... " );

    // Initialize Container keys
    ATH_MSG_DEBUG( "Inizializing containers:"        );
    ATH_MSG_DEBUG( "    ** " << m_JetContainerKey  );
    ATH_CHECK (m_JetContainerKey.initialize ());

    // Retrieve tool
    ATH_MSG_DEBUG( "Retrieving the GNN tool"        );
    ATH_CHECK (m_GNNTool.retrieve());

    // Book histograms
    ATH_MSG_DEBUG( "Booking histograms"        );
    ATH_CHECK (book (TH1F ("h_pdisp", "h_pdisp", 100, 0, 1))); 
    ATH_CHECK (book (TH1F ("h_pprompt", "h_pprompt", 100, 0, 1))); 

    return StatusCode::SUCCESS;
  }

  StatusCode GNNDecoratorAlg :: execute ()
  {

    const EventContext &ctx = Gaudi::Hive::currentContext();

    SG::ReadHandle<xAOD::JetContainer> jets(
      m_JetContainerKey,ctx);
    ATH_CHECK( jets.isValid() );
    ATH_MSG_DEBUG( "Retrieved " << jets->size() << " input jets..." );
 
    for (const xAOD::Jet *jet : *jets) {
        m_GNNTool->decorate(*jet);
        float pdisp = jet->auxdata<float>("GN1_pdisp_");
        float pprompt = jet->auxdata<float>("GN1_pprompt_");
        hist ("h_pdisp")->Fill(pdisp);
        hist ("h_pprompt")->Fill(pprompt);
    }

    return StatusCode::SUCCESS;
  }
}

